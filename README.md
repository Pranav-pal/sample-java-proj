# Sample Java proj
- Created a new maven project using intelliJ called helloworld with location: ~/Downloads/helloworld with the following archtype:
project
|-- pom.xml
`-- src
    |-- main
    |   `-- java
    |           `-- hello.java (Simple java class that prints "hello" when run)
    |-- site
    |   `-- site.xml
    `-- test
        `-- java (JUnit Framework, no test files in this case because its just a sample project)

- Then created a new java class in ~/Downloads/helloworld/src/main/java called hello.java and printed hello in this class.
- Compiled,tested and packaged the project into an executable jar file, installed using mvn commands
- Then, I initialised the maven project into a git repository using the tools in intelliJ
- staged and committed the changes using the add and commit commands
- Pushed the changes and thereby the project to this git repository
